<?php
/*
Template Name: Menu
*/
?>
<?php get_header();?>

<div id="content-wrapper">     
    <div class="row">
        <div class="container"> <!-- inner-container -->
            <header class="col-md-12" id="page-header">
                <h1 class="page-title"><?php single_post_title(); ?></h1>
            </header>
            
            <div class="col-md-12" id="content-column"> 
         <!-- START ENTREE LOOP -->

         <div class="col-md-12 menu-section-header top">
          <h2>Starters</h2>
         </div>  
          <?php 
              $my_args = array('post_type' => 'food-item',
                                'meta_key' => 'starter',
                                'meta_value' => true,
                                'meta_key' => 'show_in_menu',
                                'meta_value' => true,
                                'posts_per_page' => -1);
               
               $my_query = new WP_Query( $my_args ); ?>

          <?php   if ( $my_query->have_posts() ) :
                while ( $my_query->have_posts() ) :
                $my_query->the_post();?>   
      
      <div class="col-md-6 menu-column">
       <?php 
          $food_name = get_post_meta($post->ID, 'food_name', true);
          $food_desc = get_post_meta($post->ID, 'food_description', true);
          $food_price = get_post_meta($post->ID, 'food-price', true);
          $food_image = get_post_meta($post->ID, 'food_picture', true);?>
        
        <div class="main-menu-wrapper">
          <a class="swipebox" title="<?php _e(the_title()); ?>" href="<?php echo wp_get_attachment_url( $food_image ); ?>"><img class="img-responsive menu-pic" src="<?php echo wp_get_attachment_url( $food_image ); ?>" alt=""></a> 
          <p class="food-name"><?php _e(the_title()); ?></p>
          <?php _e(the_content()); ?>
          <p class="food-price"><?php echo '￥'.$food_price; ?></p>
        </div>
      </div>    

        <?php endwhile; wp_reset_postdata();?> 
    
    <?php endif; ?> 
    <!-- END ENTREE LOOP -->

    <!-- START MAINS LOOP -->

         <div class="col-md-12 menu-section-header">
          <h2>Mains</h2>
         </div> 

          <?php 
              $my_args = array('post_type' => 'food-item',
                                'meta_key' => 'show_in_menu',
                                'meta_value' => true,
                                'meta_key' => 'main',
                                'meta_value' => true,
                                'posts_per_page' => -1);
               
               $my_query = new WP_Query( $my_args ); ?>

          <?php   if ( $my_query->have_posts() ) :
                while ( $my_query->have_posts() ) :
                $my_query->the_post();?> 


         <?php  $food_name = get_post_meta($post->ID, 'food_name', true);
                $food_desc = get_post_meta($post->ID, 'food_description', true);
                $food_price = get_post_meta($post->ID, 'food-price', true);
                $food_image = get_post_meta($post->ID, 'food_picture', true);?>  
      
      <div class="col-md-6 menu-column">       
        <div class="main-menu-wrapper">
          <a class="swipebox" title="<?php _e(the_title()); ?>" href="<?php echo wp_get_attachment_url( $food_image ); ?>"><img class="img-responsive menu-pic" src="<?php echo wp_get_attachment_url( $food_image ); ?>" alt=""></a> 
          <p class="food-name"><?php _e(the_title()); ?></p>
          <?php _e(the_content()); ?>
          <p class="food-price"><?php echo '￥'.$food_price; ?></p>
        </div>
      </div>    

        <?php endwhile; wp_reset_postdata();?> 
    
    <?php endif; ?> 
    <!-- END MAINS LOOP -->




    </div> <!-- END CONTENT COLUMN -->     
  </div>
  </div>
</div> <!-- /PAGE-CONTAINER -->

<?php get_footer(); ?>    