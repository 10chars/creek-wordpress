        <footer class="col-md-12" id="creek-footer">
            <div id="contact-details">           
            <p class="address"><a href="http://tinyurl.com/creek-map-link">
                    <?php _e("<!--:en-->Shizuoka, Hamamatsu, Sakanamachi 313-12, Sakanamachi Building B1<!--:--><!--:ja-->〒430-0932 静岡県浜松市肴町313番地12肴町ﾋﾞﾙB1<!--:-->"); ?>
            </a></p>
            <p class="email"><a href="mailto:creek.hamamatsu@gmail.com">creek.hamamatsu@gmail.com</a></p>
            <p class="phone"><a href="tel:0534573838">(053) 457-3838</a></p>
            <div id="footer-social">
                <a href="http://www.facebook.com/pages/Casual-Diner-CREEK/172259039646383"><i class="fa fa-facebook fa-2x"></i></a>
                <a href="http://twitter.com/CreekHamamatsu"><i class="fa fa-2x fa-twitter"></i></a>
                <a href="http://plus.google.com/u/0/b/105424183852461862212/105424183852461862212/about" rel="author"><i class="fa fa-google-plus fa-2x"></i></a>
            </div>
            </div>            
        </footer> 
        </div> <!-- /inner-container -->
    </div> <!-- /page-wrapper -->


<!-- CREEK SCRIPTS -->
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/creek-scripts.js" type="text/javascript"></script>

<!-- SWIPEBOX -->
<script type="text/javascript">
    ;( function( $ ) {

         $( '.swipebox' ).swipebox({
            hideBarsOnMobile : false
        });

    } )( jQuery );
</script>
<!-- SCROLL -->
<script>
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

</script>

<!-- BOOTSLIDER -->
<script>
    $(document).ready(function() {
        var slider = new bootslider('#bootslider', {
            animationIn: "fadeInUp",
            animationOut: "flipOutX",
            timeout: 5000,
            autoplay: true,
            preload: true,
            pauseOnHover: true,
            thumbnails: false,
            pagination: true,
            mousewheel: false,
            keyboard: true,
            touchscreen: true
        });
        slider.init();

        var $bsslide = $('.bootslider .active');
        var $bsimg = $('.bootslider .bs-background img');

        var imgratio = 1920 / 850; // Change this ratio to your image size

        function setSize() {
            var ratio = $bsslide.width() / $bsslide.height();

            if (ratio >= imgratio) {
                $bsimg.removeClass().addClass('fullwidth');
            }
            else {
                $bsimg.removeClass().addClass('fullheight');
            }
        }

        $(window).resize(function() {
            setSize();
        }).trigger('resize');
    });
</script>

<?php wp_footer(); ?>
</body>
</html>