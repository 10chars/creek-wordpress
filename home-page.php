<?php
/*
Template Name: Homepage
*/
?>
<?php get_header();?>

<!-- SILDER -->
<div class="bootslider" id="bootslider">
            <!-- Bootslider Loader -->
            <div class="bs-loader">
                <img src="<?php echo get_template_directory_uri(); ?>/includes/img/loader.gif" width="31" height="31" alt="Loading.." id="loader" />
            </div>
            <!-- /Bootslider Loader -->

            <!-- Bootslider Container -->
            <div class="bs-container">  

                <!-- Bootslider Slide 1-->
                <div class="bs-slide active" data-animate-in="fadeInLeft" data-animate-out="bounceOutDown">
                    <div class="bs-foreground">
                         <div class="text-center heading-center">
                            <div class="heading heading-black" data-animate-in="fadeIn" data-animate-out="bounceOutDown" data-delay="800">                        
                                <div class="slide-text">
                                  <div class="small-text">Always</div>
                                  <div class="main-slide-text">Good people. <span>Good music.</span> Good food.</div> 
                                </div>                       
                            </div>
                        </div>
                    </div>
                    <div class="bs-background">
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/img/slide2-sm.jpg" alt="" />
                    </div>
                </div>
                <!-- /Bootslider Slide 1-->

                <!-- Bootslider Slide 2-->
                <div class="bs-slide" data-animate-in="fadeInLeft" data-animate-out="bounceOutDown">
                    <div class="bs-foreground">
                         <div class="text-center heading-center">
                            <div class="heading heading-special" data-animate-in="fadeIn" data-animate-out="bounceOutDown" data-delay="800">
                                <p class="slide-text"><span>FB</span><?php _e("<!--:en--> SHARE CAMPAIGN!<!--:--><!--:ja--> シェアキャンペーン<!--:-->"); ?></p>
                                <a href="#"><div class="slide-button"><i class="fa fa-facebook"></i>SHARE & WIN<i class="fa fa-angle-double-right"></i></div></a>                        
                            </div>            
                        </div>
                    </div>
                    <div class="bs-background">
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/img/slide8-sm.jpg" alt="" />
                    </div>
                </div>
                <!-- /Bootslider Slide 2-->
                <!-- Bootslider Slide 2-->
                <div class="bs-slide" data-animate-in="fadeInLeft" data-animate-out="bounceOutDown">
                    <div class="bs-foreground">
                         <div class="text-center heading-center">
                            <div class="heading heading-review" data-animate-in="fadeIn" data-animate-out="bounceOutDown" data-delay="1200">
                                <p class="review-text"><?php _e("<!--:en-->クリークの皆さんいつもありがとうございます♡<!--:--><!--:ja-->クリークの皆さんいつもありがとうございます♡<!--:-->"); ?></p>
                                <p class="review-author"><?php _e("<!--:en-->Hamayuki, 浜松<!--:--><!--:ja-->Hamayuki, 浜松<!--:-->"); ?></p>
                                <a href="#reviews-anchor"><div class="slide-button" style="background-color: orange;">口コミ</div></a>                        
                            </div>            
                        </div>
                    </div>
                    <div class="bs-background">
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/img/slide11.jpg" alt="" />
                    </div>
                </div>
                <!-- /Bootslider Slide 2-->


                <!-- Bootslider Slide 3 -->
                <div class="bs-slide" data-animate-in="fadeInLeft" data-animate-out="bounceOutDown">
                    <div class="bs-foreground">
                        <div class="text-center heading-center">
                        </div>          
                    </div>
                    <div class="bs-background">
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/img/slide4-sm.jpg" alt="" />
                    </div>
                </div>
                <!-- /Bootslider Slide 3-->
                
            </div>
            <!-- /Bootslider Container -->

            <!-- Bootslider Controls -->
            <div class="text-center">
                <div class="bs-controls">
                    <a href="javascript:void(0);" class="bs-prev">&lt;</a>
                    <a href="javascript:void(0);" class="bs-next">&gt;</a>
                </div>
            </div>
            <!-- /Bootslider Controls -->

            <!-- Bootslider Pagination -->
            <div class="bs-pagination text-center text-alizarin">
                <ul class="list-inline"></ul>
            </div>
            <!-- /Bootslider Pagination -->

            <!-- Bootslider Thumbnails -->
            <div class="bs-thumbnails text-center text-alizarin">
                <ul class=""></ul>
            </div>
            <!-- /Bootslider Thumbnails -->
        </div> <!-- END SLIDER -->


        <div class="col-md-12" id="slider-bottom">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAIC‌​RAEAOw==" id="ny" alt="newyork">
              A taste of New York in Hamamatsu.    
        </div>
        <div class="container hide-desktop" id="mobile-call-wrapper">
          <a href="tel:0534573838"><div class="col-md-12" id="mobile-call-button"><?php _e("<!--:en-->Call Now<!--:--><!--:ja-->すぐ電話する<!--:-->"); ?> <i class="fa fa-phone"></i></div></a>
        </div>
        <div class="container" id="grid-container"> <!-- grid container -->

          <div class="col-md-8"> <!-- left-hand-col -->
            <div class="col-sm-6 top-col">
              <div class="homepage-box hours-box">
              
              <p class="hours-text"><?php _e("<!--:en-->Lunch<!--:--><!--:ja-->Lunch<!--:-->"); ?></p>        
              <p class="hours-text-small"><?php _e("<!--:en-->Monday-Saturday<!--:--><!--:ja-->月曜日〜土曜日<!--:-->"); ?></p>
              <p class="hours-text-small">11:30~15:00</p>
              <p class="hours-text"><?php _e("<!--:en-->Dinner<!--:--><!--:ja-->Dinner<!--:-->"); ?></p>        
              <p class="hours-text-small"><?php _e("<!--:en-->Monday-Sunday<!--:--><!--:ja-->月曜日〜日曜日<!--:-->"); ?></p>
              <p class="hours-text-small">18:00~1:00</p>  
                     
              <a href="http://www.creek-hamamatsu.com/access/"><div class="homepage-box-button">Reservation</div></a>
              </div>
            </div>
            <div class="col-sm-6 top-col">
              <div class="homepage-box menu-box">
                 <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAIC‌​RAEAOw==" id="menu-icon" alt="">  
                 <a href="http://www.creek-hamamatsu.com/menu/"><div class="homepage-box-button">Our Menu</div></a>
              </div>
            </div>
            <a href="http://www.creek-hamamatsu.com/gallery/">
              <div class="col-sm-6 top-col">  
              <figure class="effect-sarah img img-responsive">
                    <img class="img img-responsive" src="<?php echo get_template_directory_uri(); ?>/includes/img/gallery2.jpg" alt=""/>
                    <figcaption>
                        <h2>CREEK<span>Gallery</span></h2>                 
                        <p><?php _e("<!--:en-->View our picture gallery<!--:--><!--:ja-->CREEKのギャラリーへ<!--:-->"); ?></p>        
                    </figcaption>           
              </figure>
            </div>
            </a>
            <a href="http://www.creek-hamamatsu.com/blog/">
            <div class="col-sm-6 top-col">  
              <figure class="effect-sarah img img-responsive">
                    <img class="img img-responsive" src="<?php echo get_template_directory_uri(); ?>/includes/img/blog2.jpg" alt=""/>
                    <figcaption>
                        <h2>CREEK<span>Blog</span></h2>                 
                        <p><?php _e("<!--:en-->Check out our blog<!--:--><!--:ja-->CREEKのブログへ<!--:-->"); ?></p>        
                    </figcaption>           
              </figure>
            </div>
          </a>
          </div> <!-- /left-hand-col -->
          
          <div class="col-md-4" id="rhc"> <!-- right-hand-col -->
            <img class="img img-responsive" src="<?php echo get_template_directory_uri(); ?>/includes/img/concept.jpg" alt="">
            <div id="concept-text">
                  <?php _e("<!--:en-->At Creek, we've done our very best to bring you that NY feeling right here in Hamamatsu. Whether it's dinner with friends, a date or just a few quiet drinks and chilling to good tunes - we've got you covered.<!--:--><!--:ja-->ニューヨークスタイル浜松に近代的ＮＹスタイルのカジュアル空間が登場！地下に降りて扉をあければ…そこは、非日常的な空間が。<!--:-->"); ?>
                        
                  <p id="signature">
                    Ogawa Hiroshi, Owner
                  </p>
              </div>
          </div> <!-- /right-hand-col -->
          
        </div> <!-- /grid-container -->
        <a name="reviews-anchor" id="review-ap"></a>
        <div class="col-md-12" id="reviews">
         <p id="review-title">CUSTOMER<span> REVIEWS</span></p>  
        <?php echo do_shortcode('[bne_testimonials_slider image_style="square" nav="false" arrows="false"]' ); ?>      
        <!-- <div class="review-arrow"></div> -->
        </div>   	 

<?php get_footer(); ?>