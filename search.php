<?php get_header(); ?>
    <div id="content-wrapper">

        <div class="row">

     <div class="container"> <!-- inner-container -->
        <header class="col-md-12" id="page-header">
        <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'lukey' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </header>
    <div class="col-md-8" id="content-column">

        <?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); ?>

            <div class="standard-post-header">
                <a href="<?php the_permalink(); ?>"> <h2 class="sub-title"><?php the_title(); ?></h2> </a>
                <i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?> <i class="fa fa-clock-o"></i>12:00
            </div>

            <div class="post-content">

                <?php the_excerpt(); ?>

            </div>

		<?php endwhile; ?>

	<?php else : ?>

        <div class="standard-post-header">
            <h2 class="sub-title"> <?php _e( 'Nothing Found', 'lukey' );?></h2>
        </div>
        <div class="post-content">
            <?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'lukey' ); ?>
            <a href=href="javascript: history.go(-1)" class="button-flat">BACK</a>
        </div>
	
	<?php endif; // end of loop. ?>
    </div><!--/end-content-->
        <?php get_sidebar(); ?>
    </div><!--/inner-container-->
    </div><!--/end-main-row-->
</div><!--/page-container-->

<?php get_footer(); ?>