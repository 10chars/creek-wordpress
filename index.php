<?php get_header(); ?>
<!-- INDEX PAGE START -->
<div id="content-wrapper">

    <div class="row">

        <div class="container"> <!-- inner-container -->
            <header class="col-md-12" id="page-header">
                <h1 class="page-title"><?php single_post_title(); ?></h1>
            </header>
            <div class="col-md-8" id="content-column">

                <!-- SETUP PAGINATION -->
                <?php
                if ( get_query_var('paged') ) {
                    $paged = get_query_var('paged');
                } else if ( get_query_var('page') ) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }

                $my_args = array(
                    'post_type' => '',
                    'posts_per_page' => 6,
                    'paged' => $paged);

                $my_query = new WP_Query( $my_args );?>

                <!-- LOOP -->
                <?php   if ( $my_query->have_posts() ) :
                    while ( $my_query->have_posts() ) :
                        $my_query->the_post(); ?>
                        <article class="single-post-container">
                        <!-- STANDARD HEADER ARTICLE -->
                        <div class="standard-post-header">
                            <a href="<?php the_permalink(); ?>"> <h2 class="sub-title"><?php the_title(); ?></h2> </a>
                            <i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?> <i class="fa fa-clock-o"></i>12:00
                        </div>

                        <div class="post-content">

                            <?php the_content(); ?>
                            
                        <a href="<?php the_permalink(); ?>">
                        <div class="read-more-button">
                        <?php _e("<!--:en-->Read More<!--:--><!--:ja-->もっと見る<!--:-->"); ?>
                        </div>
                        </a>
                        </div>
                        </article>
                    <?php endwhile; wp_reset_postdata();?>

                    <?php if ( function_exists('vb_pagination') ) {
                    vb_pagination( $my_query );} ?>
                <?php else: ?>
                    <h2>Not found</h2>
                <?php endif; ?>

            </div>   <!-- /content-column -->

            <?php get_sidebar(); ?>

        </div> <!-- /inner-container -->

    </div> <!-- main-row -->

</div> <!-- /page-wrapper -->





<?php get_footer(); ?>
