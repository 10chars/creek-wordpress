<?php
/**
 * User: Lukey
 * Date: 4/19/14
 * 
 */?>

<aside class="col-md-3" id="sidebar-access">
   
    <article class="widget" id="photo-widget">
    	<img src="<?php echo get_template_directory_uri(); ?>/includes/img/creek-hamamatsu-outside.jpg" class="img img-responsive" alt="">
	</article>

	<article class="widget" id="hot-pepper">
		<h3 class="underline-title">COUPON</h3>
		<a href="http://www.hotpepper.jp/strJ001050000/"><img src="<?php echo get_template_directory_uri(); ?>/includes/img/hotpepper.jpg" class="img img-responsive" alt=""></a>
	</article>
	
	<!-- Social Widget -->
	<article class="widget" id="social-widget">
		<h3 class="underline-title">FOLLOW US</h3>		
		<div class="social-widget-wrapper">
			<a href="http://plus.google.com/u/0/b/105424183852461862212/105424183852461862212/about" rel="author"><i class="fa fa-google-plus fa-3x"></i></a>
			<a href="http://twitter.com/CreekHamamatsu"><i class="fa fa-twitter fa-3x"></i></a>
			<a href="http://www.facebook.com/pages/Casual-Diner-CREEK/172259039646383"><i class="fa fa-facebook fa-3x"></i></a>		
		</div>
	</article>

	
	

</aside> <!-- end sidebar -->