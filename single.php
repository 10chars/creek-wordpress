<?php
/**
 * User: Lukey
 * Date: 4/19/14
 * 
 */
get_header(); ?>

<div id="content-wrapper">
      
    <div class="row">

        <div class="container"> <!-- inner-container -->
            <header class="col-md-12" id="page-header">
                <h1 class="page-title">BLOG</h1>
            </header>
            
            <div class="col-md-8" id="content-column">

                <?php while(have_posts()) : the_post(); ?>

                        <!-- STANDARD HEADER ARTICLE -->
                        <div class="single-standard-post-header">
                            <a href="<?php the_permalink(); ?>"> <h2 class="sub-title"><?php the_title(); ?></h2> </a>
                            <i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?> <i class="fa fa-clock-o"></i>12:00
                          <div class="social-sharing-wrapper">
                            <div class="addthis_native_toolbox"></div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52f61d3b2284299a"></script>
                          </div>
                        </div>

                        <div class="post-content">
                            <?php  the_content();   ?>                 
                            
                            <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                var disqus_shortname = 'creekhamamatsu';

                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>

                        </div>

                <?php endwhile; wp_reset_query(); ?>
            </div>   <!-- /content-column -->

            <?php get_sidebar(); ?>

        </div> <!-- /inner-container -->

    </div> <!-- main-row -->

</div> <!-- /page-wrapper -->

<?php get_footer(); ?>
