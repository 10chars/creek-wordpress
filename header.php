<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'RIGHT' ); ?></title>
    <link href="<?php echo get_template_directory_uri(); ?>/includes/css/creek-styles.min.css" rel="stylesheet" type="text/css" />   
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/js/respond.src.js"></script>
    <![endif]-->
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href='http://fonts.googleapis.com/css?family=Satisfy%7COswald' rel='stylesheet' type='text/css'>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42937612-2', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_head(); ?>
</head>

<body>

<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=532857810151555&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container" id="page-wrapper"> <!-- page-wrapper -->

        <div class="container" id="inner-wrapper"> <!-- inner-container -->
            
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
            <div class="container">
              <div class="navbar-header"> 
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbarHeadercollapse"> 
                  <span class="icon-bar"></span> 
                  <span class="icon-bar"></span> 
                  <span class="icon-bar"></span> 
                </button>          
                <a class="navbar-brand" href="<?php echo home_url(); ?>"><img alt="" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAIC‌​RAEAOw==" id="brand-logo"></a> 
              </div>
          
              <div class="collapse navbar-collapse navbarHeadercollapse"> 
               <?php    wp_nav_menu( array(
                        'menu' => 'top_menu',
                        'depth' => 2,
                        'container' => false,
                        'menu_class' => 'nav navbar-nav navbar-right',
                        'walker' => new wp_bootstrap_navwalker())
                        );?>                     
              </div> <!-- ENDOFTHEWORLD -->
            </div> 
        </nav>