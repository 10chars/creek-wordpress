<?php
/**
 * User: Lukey
 * Date: 4/19/14
 * 
 */?>

<aside class="col-md-3" id="sidebar">
   
    <article class="lukey-widget" id="facebook-like-widget">
		<h3 class="underline-title">LIKE US</h3>
		<div class="lukey-widget-content facebook-widget">
		<div class="fb-like-box" data-href="http://www.facebook.com/pages/Casual-Diner-CREEK/172259039646383" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
		</div>
	</article>
	
	<!-- Tag Cloud Widget -->
	 <article class="widget">
        <h3 class="underline-title">TAGS</h3>
        <?php wp_tag_cloud('format=list&smallest=10&largest=10');?>
    </article>

	
	<!-- Social Widget -->
	<article class="widget" id="social-widget">
		<h3 class="underline-title">FOLLOW US</h3>		
		<div class="social-widget-wrapper">
			<a href="http://plus.google.com/u/0/b/105424183852461862212/105424183852461862212/about" rel="author"><i class="fa fa-google-plus fa-3x"></i></a>
			<a href="http://twitter.com/CreekHamamatsu"><i class="fa fa-twitter fa-3x"></i></a>
			<a href="http://www.facebook.com/pages/Casual-Diner-CREEK/172259039646383"><i class="fa fa-facebook fa-3x"></i></a>		
		</div>
	</article>

</aside> <!-- end sidebar -->