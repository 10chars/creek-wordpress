<?php
/*
Template Name: Coupon Page
*/
?>
<?php get_header();?>

    <div id="content-wrapper">

        <div class="row">

            <div class="container"> <!-- inner-container -->
                <header class="col-md-12" id="page-header">
                    <h1 class="page-title"><?php single_post_title(); ?></h1>
                </header>
                <div class="col-md-12" id="coupon-container">

                    <?php while(have_posts()) : the_post(); ?>

                        <?php $coupon_code = strtoupper(generatePassword()); ?>

                        <div class="coupon-wrapper">

                        <h3>Like our facebook page for a 5% off your next visit.</h3>
                        <?php echo do_shortcode('[sociallocker id="330"]<a href="#myModal" id="enter-link" role="button" data-toggle="modal">ENTRY LINK</a>[/sociallocker]'); ?>
                      
                        <script>

                        $('#enter-link').onclick($(this).hide());

                        </script>

                    <?php endwhile; wp_reset_query(); ?>
                </div>   <!-- /content-column -->

            </div> <!-- /inner-container -->

        </div> <!-- main-row -->

    </div> <!-- /page-wrapper -->


<?php get_footer(); ?>
