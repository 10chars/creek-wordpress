<?php
/*
Template Name: Access Page
*/
?>

<?php get_header();?>

    <div id="content-wrapper">

        <div class="row">

            <div class="container"> <!-- inner-container -->
                <header class="col-md-12" id="page-header">
                    <h1 class="page-title"><?php single_post_title(); ?></h1>
                </header>
                <div class="col-md-8" id="content-column">

                    <?php while(have_posts()) : the_post(); ?>
                    
                        <div class="single-post-content">
                            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                            <div id="map" style="width:100%;height:420px;"></div>
                            <script type="text/javascript">
                              var latlng = new google.maps.LatLng(34.7067043, 137.72952580000003);
                              var myOptions = {
                                zoom: 17,
                                center: latlng,
                                scrollwheel: true,
                                scaleControl: false,
                                disableDefaultUI: false,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                              };
                              var map = new google.maps.Map(document.getElementById("map"),myOptions);
                              var marker = new google.maps.Marker({
                                map: map, 
                                position: map.getCenter()
                              });      
                              var contentString = '';
                              var infowindow = new google.maps.InfoWindow({
                                content: contentString
                              });
                                          
                              google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map,marker);
                              });
                              
                              infowindow.open(map,marker);
                            </script>             
                        </div>
                        <?php the_content(); ?>
                        
                        <div id="contact-details-wrapper">
                        <h3>Reservations</h3> 
                        <h4 class="contact-heading"><?php _e("<!--:en-->PHONE/FAX<!--:--><!--:ja-->PHONE/FAX<!--:-->"); ?></h4>
                        <p>053-457-3838</p>
                        <h4 class="contact-heading"><?php _e("<!--:en-->E-MAIL<!--:--><!--:ja-->E-MAIL<!--:-->"); ?></h4>
                        <p>creek@email.com</p>   
                        <h4 class="contact-heading"><?php _e("<!--:en-->ADDRESS<!--:--><!--:ja-->ADDRESS<!--:-->"); ?></h4>
                        <p><?php _e("<!--:en-->〒430-0932 Shizuoka, Hamamatsu,Sakanamachi 313-12, Sakanamachi Building Basement<!--:--><!--:ja-->〒430-0932 静岡県浜松市肴町313番地12肴町ﾋﾞﾙB1<!--:-->"); ?></p>
                        <h4 class="contact-heading"><?php _e("<!--:en-->HOURS<!--:--><!--:ja-->HOURS<!--:-->"); ?></h4>
                        <h4 class="contact-sub-heading"><?php _e("<!--:en-->LUNCH<!--:--><!--:ja-->ランチ<!--:-->"); ?></h4>
                        <p><?php _e("<!--:en-->Monday-Saturday: <!--:--><!--:ja-->月曜日〜土曜日: <!--:-->"); ?>11:30~15:00</p>
                        <h4 class="contact-sub-heading"><?php _e("<!--:en-->DINNER<!--:--><!--:ja-->ディナー<!--:-->"); ?></h4>
                        <p><?php _e("<!--:en-->Monday-Sunday: <!--:--><!--:ja-->月曜日〜日曜日: <!--:-->"); ?>18:00~1:00</p>
                                       
                        <div id="access-button-wrapper">
                        <h4 class="contact-heading">MOBILE<i class="fa fa-mobile"></i></h4>    
                        <a href="http://www.google.com/maps/place/%E3%80%92430-0932+Shizuoka-ken,+Hamamatsu-shi,+Naka-ku,+Sakanamachi,+313%E2%88%9212+%E8%82%B4%E7%94%BA%E3%83%93%E3%83%AB/@34.7065077,137.7287016,17z/data=!3m1!4b1!4m2!3m1!1s0x601ade7ca55d8505:0x17978753c8feaaa0"><div id="access-map-button"><?php _e("<!--:en-->Google Maps<!--:--><!--:ja-->マップのアプリで開く<!--:-->"); ?></div></a>
                        <a href="http://line.me/R/msg/text/?<?php the_title(); ?>%0D%0A<?php the_permalink(); ?>">
                        <div id="access-line-button"><?php _e("<!--:en-->LINEで送る<!--:--><!--:ja-->LINEで送る<!--:-->"); ?></div>  
                        </a>
                        </div>              
                        </div>
                    <?php endwhile; wp_reset_query(); ?>         
                
                </div>   <!-- /content-column -->

                <?php get_sidebar('access'); ?>

            </div> <!-- /inner-container -->

        </div> <!-- main-row -->

    </div> <!-- /page-wrapper -->





<?php get_footer(); ?>

<?php get_footer(); ?>