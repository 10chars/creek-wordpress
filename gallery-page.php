<?php
/*
Template Name: Gallery
*/
?>
<?php get_header();?>

<div id="content-wrapper">
      
    <div class="row">

        <div class="container"> <!-- inner-container -->
            <header class="col-md-12" id="page-header">
                <h1 class="page-title"><?php single_post_title(); ?></h1>
            </header>
                <div id="filters">
                  <!-- <div class="ui-group"> -->
                  <!-- <div class="button-group js-radio-button-group" data-filter-group="type"> -->
                    <p class="filter">FILTER<span>:</span></p>
                    <p class="button is-checked" data-filter="">ALL<span>/</span></p>
                    <p class="button" data-filter=".shop">SHOP<span>/</span></p>
                    <p class="button" data-filter=".food">FOOD<span>/</span></p>
                    <p class="button" data-filter=".dj">MUSIC<span>/</span></p>
                  <!-- </div> -->
                <!-- </div> -->
              </div>
            <div class="col-md-12" id="gallery-content-column">
            <div class="row" id="posts">
                  
              <!-- START SHOP LOOP --> 
              <?php 
             
              $my_args = array('post_type' => 'image-gallery',
                                'meta_key' => 'gallery_type',
                                'meta_value' => 'shop',
                                'posts_per_page' => -1);
               
               $my_query = new WP_Query( $my_args ); ?>
          
          <?php while ( $my_query->have_posts() ) :            
                
                $my_query->the_post();
                $gallery = get_post_gallery_images($post);
                $count = 1;
                foreach( $gallery as $image ){ 

                      $fullsize = str_replace("-150x150", "", $image); 
                      ?>           
                      <div class="post item span3 shop"> 
                       <a class="swipebox" title="<?php echo the_title().'-'.$count++; ?>" href="<?php echo $fullsize; ?>"><img src="<?php echo $fullsize; ?>" class="img img-responsive" alt=""></a>
                      </div>
                    
               <?php } ?>

            <?php endwhile; wp_reset_postdata();?>     
            <!-- END SHOP LOOP-->

            <!-- START DJ LOOP --> 
              <?php 
             
              $my_args = array('post_type' => 'image-gallery',
                                'meta_key' => 'gallery_type',
                                'meta_value' => 'other',
                                'posts_per_page' => -1);
               
               $my_query = new WP_Query( $my_args ); ?>

          <?php while ( $my_query->have_posts() ) :            
                
                $my_query->the_post();
                $gallery = get_post_gallery_images($post);
 
                foreach( $gallery as $image ){ 
               
                      $fullsize = str_replace("-150x150", "", $image); 
                      ?>           
                      <div class="post item span3 dj"> 
                       <a class="swipebox" title="<?php echo the_title(); ?>" href="<?php echo $fullsize; ?>"><img src="<?php echo $fullsize; ?>" class="img img-responsive" alt=""></a>
                      </div>
                                          
               <?php } ?>

            <?php endwhile; wp_reset_postdata();?>     
            <!-- END DJ LOOP-->
            
            <!-- START FOOD LOOP --> 
              <?php 
             
              $my_args = array('post_type' => 'image-gallery',
                                'meta_key' => 'gallery_type',
                                'meta_value' => 'food',
                                'posts_per_page' => -1);
               
               $my_query = new WP_Query( $my_args ); ?>

          <?php while ( $my_query->have_posts() ) :            
                
                $my_query->the_post();
                $gallery = get_post_gallery_images($post);
 
                foreach( $gallery as $image ){ 
               
                      $fullsize = str_replace("-150x150", "", $image); 
                      ?>           
                      <div class="post item span3 food"> 
                       <a class="swipebox" title="<?php echo the_title(); ?>" href="<?php echo $fullsize; ?>"><img src="<?php echo $fullsize; ?>" class="img img-responsive" alt=""></a>
                      </div>
                                          
               <?php } ?>

            <?php endwhile; wp_reset_postdata();?>     
            <!-- END FOOD LOOP-->

           </div>  <!-- END POSTS ROW -->  
          </div> <!-- END CONTENT COLUMN -->     
      </div>
      </div>

</div> <!-- /PAGE-CONTAINER -->

<?php get_footer(); ?>    